<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.tovo.applications.annonces.util.TokenHelper" %>

<c:import url = "../_header.jsp"/>

<main>
  <div class="container">
    <div class="row centered-form">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-4 m-auto">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-color text-center">Login</h3>
         </div>
         <div class="panel-body">
          <form method="POST" action="#">
          <c:set var="csrfToken" value="${ TokenHelper.generateCsrfToken() }" />
          <c:set var="_csrfToken" value="${csrfToken}" scope="session" />
          <input type="hidden" value="${csrfToken}" name="${ TokenHelper.CSRF_TOKEN_VALUE_NAME }" />

            <div class="form-group">
              <input type="text" name="username" id="username" class="form-control input-sm" placeholder="Username">
            </div>
            <div class="form-group">
              <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
            </div>
            <div class="text-center">
              <label class="checkbox">
                <input type="checkbox" value="true" name="stay-logged-in">
                Remember me
              </label>
            </div>
            <input type="submit" value="Register" class="btn btn-success btn-block btn-form">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

</main>





<c:import url = "../_footer.jsp"/>
