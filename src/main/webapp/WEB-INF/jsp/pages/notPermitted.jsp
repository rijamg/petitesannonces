<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url = "../_header.jsp"/>

<div class="container">
  <h1><c:out value = "${ message }"/></h1>
</div>

<c:import url = "../_footer.jsp"/>
