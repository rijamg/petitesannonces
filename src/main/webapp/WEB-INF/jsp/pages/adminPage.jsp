<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.tovo.applications.annonces.util.TokenHelper" %>

<c:import url = "../_header.jsp"/>

<main>
  <div class="container">
    <div class="row centered-form">
      <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-4 m-auto">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-color text-center">Veuillez saisir une nouvelle categorie</h3>
          </div>
          <form method="POST" action="adminPage">
          <c:set var="csrfToken" value="${ TokenHelper.generateCsrfToken() }" />
          <c:set var="_csrfToken" value="${csrfToken}" scope="session" />
          <input type="hidden" value="${csrfToken}" name="${ TokenHelper.CSRF_TOKEN_VALUE_NAME }" />
            <div class="panel-body">
                <div class="form-group">
                  <label for="categories">Categories:</label>
                  <input type="text" name="categorie" id="categories" class="form-control input-sm" placeholder="Nouvelle Categorie">
                </div>
              <input type="submit" value="Valider" class="btn btn-form btn-success btn-block">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<div class="container mt-5">
  <table class="table">
    <thead>
      <tr>
        <th>Nom</th>
        <th>Role</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="user" items="${ userList }">
        <tr>
          <td><c:out value="${ user.username }"/></td>
          <td><c:out value="${ user.userRole.getRoleTitle() }"/></td>
          <td>
            <a href="<c:url value="/adminPage?id=${user.id}" />">
              Delete
            </a>
            <a href="<c:url value="/modifyUser?id=${user.id}" />">
                Modify
              </a>
          </td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</div>

<div class="container mt-5">
  <table class="table">
    <thead>
      <tr>
        <th>Categorie</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="categorie" items="${ sessionScope.categories }">
        <tr>
          <td><c:out value="${categorie.categorieTitle}"/></td>
          <td>
            <a href="#">
              Delete
            </a>
            <a href="#">
                Modify
              </a>
          </td>
        </tr>
      </c:forEach>

    </tbody>
  </table>
</div>

</main>



<c:import url = "../_footer.jsp"/>
