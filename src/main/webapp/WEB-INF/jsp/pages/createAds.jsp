<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.tovo.applications.annonces.util.TokenHelper" %>

<c:import url = "../_header.jsp"/>

<main>
  <div class="container">
    <div class="row centered-form">
    <div class="col-xs-12 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-4 m-auto">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title text-color text-center">Veuillez déposer votre annonce ici</h3>
         </div>
         <div class="panel-body">
          <form method="POST" action="createAds" enctype="multipart/form-data">
          <c:set var="csrfToken" value="${ TokenHelper.generateCsrfToken() }" />
          <c:set var="_csrfToken" value="${csrfToken}" scope="session" />
          <input type="hidden" value="${csrfToken}" name="${ TokenHelper.CSRF_TOKEN_VALUE_NAME }" />

            <div class="form-group">
              <input type="text" name="adsTitle" id="adsTitle" class="form-control input-sm" placeholder="Titre">
            </div>
            <div class="form-group">
              <label for="sel1">Categories:</label>
              <select name="categories" class="form-control" id="sel1">
                <c:forEach var="categorie" items="${categories}">
                  <option value="${categorie.getId()}">${categorie.getCategorieTitle()}</option>
                </c:forEach>
              </select>
            </div>

            <div>
              <div class="form-group">
                <!-- <textarea name="body" id="body" class="form-control input-sm" placeholder="Contenu"></textarea> -->
                <textarea name="adsBody" id="editor" rows="10" cols="80"></textarea>
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <input type="file" name="photos" multiple/>
              </div>
            </div>

            <input type="submit" value="Valider" class="btn btn-form btn-success btn-block">
          </form>

        </div>
      </div>
    </div>
  </div>
</div>

</main>




<c:import url = "../_footer.jsp"/>
