<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url = "../_header.jsp"/>

<main>
  <div class="container mt-5">
    <div class="row">
      <div id="carouselExampleIndicators" class="carousel slide m-auto col-6" data-ride="carousel">
        <div class="carousel-inner">
        <c:forEach var="photo" items="${ ads.images }">
          <c:set var="firstId" value="${ ads.images[0].getId() }"/>
          <c:if test= "${ photo.getId() == firstId }">
            <c:set value="active" var="cssClass"></c:set>
          </c:if>
          <c:if test= "${ photo.getId() != firstId }">
            <c:set value="" var="cssClass"></c:set>
          </c:if>
          <div class="carousel-item ${ cssClass }">
            <img class="d-block w-50" src="data:image/jpg;base64,<c:out value="${ photo.getPhoto() }"/>" alt="slide">
          </div>
        </c:forEach>
        </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    <div class="container mt-5 col-6">
      <div class="row ">
        <div class="card mt-2 m-auto" style="width: 56rem; height: 36rem">
          <div class="card-body">
            <h5 class="card-title">Card title : <c:out value="${ ads.title }"/></h5>
            <p class="card-text">Categorie : <c:out value="${ ads.categories.categorieTitle }"/></p>
            <p class="card-text">Date : : <c:out value="${ ads.creationDate }"/></p>
            <div class="form-group">
              <textarea disabled name="body" id="editor" class="form-control input-sm"
                placeholder="Contenu" style="height: 18rem"><c:out value="${ ads.body }"/></textarea>
            </div>
            <c:if test="${ sessionScope.user.username.equals(ads.user.username)}">
            <a href="<c:url value="/vuead?idtodelete=${ads.id}" />">
              <!-- <a href="<c:url value="/article/delete?id=${article.id}" />" > -->
                Delete
              </a>
              <a href="#">
                <!-- <a href="<c:url value="/article/delete?id=${article.id}" />" > -->
                  Modify
              </a>
            </c:if>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</main>




<c:import url = "../_footer.jsp"/>
