<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url = "../_header.jsp"/>

<main>

  <c:import url = "../_pagination.jsp"/>

    <div class="container mt-5">
      <div class="row justify-content-between mr-1 ml-1">
        <c:forEach var="ads" items="${adsList}">
          <div class="card mt-2" style="width: 18rem;">
            <c:if test="${ empty ads.images[0].getPhoto() }">
              <img class="card-img-top" src="<c:url value="/static/resources/noimage.png" />" alt="Card image cap" height="400">
            </c:if>
            <c:if test="${ not empty ads.images[0].getPhoto() }">
            <img class="card-img-top" src="data:image/jpg;base64,<c:out value="${ ads.images[0].getPhoto() }"/>" alt="Card image cap" height="400">
            </c:if>
            <div class="card-body">
              <h5 class="card-title"><c:out value="${ ads.title }"/></h5>
              <p class="card-text"><c:out value="${ ads.categories.getCategorieTitle() }"/></p>
              <p class="card-text"><c:out value="${ ads.getCreationDate() }"/></p>
              <a href="<c:url value="/vuead?id=${ads.id}" />" class="btn btn-success">En voir plus</a>
            </div>
          </div>
        </c:forEach>
      </div>
    </div>

  <c:import url = "../_pagination.jsp"/>
</main>

<c:import url = "../_footer.jsp"/>
