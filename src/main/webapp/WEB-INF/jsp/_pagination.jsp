<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container mt-5">
  <nav aria-label="Page navigation" >
    <c:set var="lastPage" value="${Math.ceil(articleCount / itemsPerPage)}" />

    <ul class="pagination">
    <c:if test="${currentPage > 1}">
    <li class="page-item">
      <a class="page-link"
      href="<c:url value="?page=${currentPage - 1}&idCategorie=${idCategorie}&search=${search}"/> " aria-label="Previous"
      >Previous</a>
    </li>
    </c:if>
    <c:if test="${currentPage < lastPage}">
    <li class="page-item">
      <a class="page-link"
      href="<c:url value="?page=${currentPage + 1}&idCategorie=${idCategorie}&search=${search}"/> " aria-label="Next"
      >Next page</a>
    </li>
    </c:if>
      <c:forEach var="page" begin="1" end="${lastPage}">
        <li class="page-item">
          <a class="page-link ${page == currentPage ? 'active' : ''}"
            href="<c:url value="?page=${page}&idCategorie=${idCategorie}&search=${search}"/>"
            aria-label="Page <c:out value="page" />"
            aria-current="page">
            <c:out value="${page}" />
          </a>
        </li>
      </c:forEach>
    </ul>
  </nav>
</div>
