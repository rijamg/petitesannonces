<%@ page
  info="List all articles"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <link rel="stylesheet" href="<c:url value="/static/styles/index.css" />">
  <title>Document</title>
</head>
<body>
  <header>
    <div class="container">
      <div class="row justify-content-between">
        <div class="row ml-1">
          <h1 class="title-name ml-4">LeSite</h1>
          <a href="<c:url value="/createAds" />"><button class="btn btn-warning my-3 ml-2 btn-color"> + Déposer une annonce</button></a>
          <c:if test="${empty sessionScope.user }">
            <a href="<c:url value="/login" />"><button class="btn btn-warning my-3 ml-2 btn-color" type="submit"> Log in</button></a>
          </c:if>
          <c:if test="${not empty sessionScope.user }">
            <a href="<c:url value="/logout" />"><button class="btn btn-warning my-3 ml-2 btn-color" type="submit"> Log out</button></a>
            <p class="my-4">Bienvenue <c:out value="${user.getUsername()}"/></p>
          </c:if>
        </div>
        <div class="mt-3 align-items-end mr-4">
          <a href="<c:url value="/register" />">
            <img src="<c:url value="/static/resources/id.png" />" width="30" height="30" alt="">
            <p class="text-connexion">Enregistrement</p>
          </a>
        </div>
      </div>
    </div>
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-color bg-form">
        <a class="navbar-brand" href="<c:url value="/" />">
          <img src="<c:url value="/static/resources/eco_green_earth_01.png" />" width="40" height="40" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link text-color" href="<c:url value="/" />">Voir tout<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link text-color dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categories
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <c:forEach var="categorie" items="${sessionScope.categories}">
                <a class="dropdown-item" href="<c:url value="/?idCategorie=${categorie.getId()}" />">${categorie.getCategorieTitle()}</a>
                </c:forEach>
              </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" method="POST" action="#">
          <c:set var="csrfToken" value="${ TokenHelper.generateCsrfToken() }" />
          <c:set var="_csrfToken" value="${csrfToken}" scope="session" />
          <input type="hidden" value="${csrfToken}" name="${ TokenHelper.CSRF_TOKEN_VALUE_NAME }" />
          <input class="form-control mr-sm-2" name="searchPost" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
  </div>
</header>
