package com.tovo.applications.annonces.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import com.tovo.applications.annonces.models.User;



@WebFilter(filterName="adminpage-filter", urlPatterns = {"/adminPage"})
public class AdminPageFilter implements Filter {

    /**
     * Filter to access /adminPage only if admin or superadmin
     * @param req,
     * @param resp,
     * @param chain,
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
        throws IOException, ServletException {
            HttpServletRequest httpRequest = (HttpServletRequest) req;

            User user = new User();
            user = (User) httpRequest.getSession().getAttribute("user");

        if (user.getUserRole().getRoleTitle().equals("admin") ||
            user.getUserRole().getRoleTitle().equals("superadmin"))
                {
                    chain.doFilter(req, resp);
            }else
            {
                String message = "not Permitted page";
                req.setAttribute("message", message);
                httpRequest.getRequestDispatcher("/WEB-INF/jsp/pages/notPermitted.jsp").forward(req, resp);
            }
    }
}
