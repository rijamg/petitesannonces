package com.tovo.applications.annonces.filter;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import com.tovo.applications.annonces.DAO.UserDAO;
import com.tovo.applications.annonces.models.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j @WebFilter(filterName="cookie-filter", urlPatterns = "/*")
public class CookieFilter implements Filter{

    @Resource(name = "jdbc/petitesannonces")
    DataSource datasource;

    private UserDAO userDAO;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void destroy() {}

    /**
     * Filter to give access by cookie if already authentified
     * @param request
     * @param response
     * @param chain
     */
    @Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        Cookie authToken = this.getAuthTokenCookie(httpRequest);

        if (authToken != null) {
            try {
                this.userDAO = new UserDAO(datasource);
                User user = userDAO.findUserByCookieToken(
                    authToken.getValue());

                if (user != null) {
                    if (user.getCookieTokenExpiration()
                            .isAfter(LocalDateTime.now())) {
                        httpRequest.getSession().setAttribute("user", user);
                        log.info("Authenticated User <{}> with token <{}>",
                            user.getUsername(),
                            user.getCookieToken());
                    }
                    else { // Token is outdated, let's remove it from the database
                        user.setCookieToken(null);
                        user.setCookieTokenExpiration(null);
                        userDAO.update(user);

                        log.info("User <{}> token's went outdated",
                            user.getUsername());
                    }
                }
                else {
                    log.info("No User were found for the token <{}>",
                        authToken.getValue());
                }
            }
            catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
        }
        chain.doFilter(request, response);



    }

    private Cookie getAuthTokenCookie(HttpServletRequest request) {
        Cookie authToken = null;

        if (request.getCookies() != null) {
            for (Cookie cookie: request.getCookies()) {
                if (cookie.getName().equals("auth-token")) {
                    authToken = cookie;
                    break;
                }
            }
        }

        return authToken;
    }

}
