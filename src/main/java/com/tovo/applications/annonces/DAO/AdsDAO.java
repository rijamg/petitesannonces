package com.tovo.applications.annonces.DAO;

import com.tovo.applications.annonces.models.Ads;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Slf4j
public class AdsDAO {

    /**
     * String Value used to find all fields of ADs Table
     */
    private static final String FIELD_LIST=
        "idads,"
        + "iduser,"
        + "idcategories,"
        + "title,"
        + "body,"
        + "creationdate ";


    /**
     * String Value used to find Ads with a given id
     */
    public static final String SELECT_BY_ID =
        "SELECT "
            + FIELD_LIST
            + "FROM ads WHERE idads = ?";

    public static final String DELETE_BY_ID =
        "DELETE FROM ads WHERE idads = ?;";


    /**
     * String Value used to find Ads with a given idCategorie
     */
    public static final String SELECT_BY_IDCATEGORIE =
    "SELECT "
        + FIELD_LIST
        + "FROM ads WHERE idcategories = ? LIMIT ?, ? ;";
    /**
     * String Value used to find all Ads
     */
    public static final String SELECT_ALL =
        "SELECT "
            + FIELD_LIST
            + "FROM ads LIMIT ?, ?";

    /**
     * String Value used to find all Ads by search
     */
    public static final String SELECT_ALL_BY_SEARCH =
        "SELECT "
            + FIELD_LIST
            + "FROM ads WHERE body LIKE ? LIMIT ?, ?";

    /**
     * String Value used to insert new Ads on Data base
     */
    public static final String INSERT =
        "INSERT INTO ads (iduser, idcategories, title, body, creationdate) VALUES (?, ?, ?, ?, ?);";

    /**
     * String Value used to count ads
     */
    public static final String SELECT_COUNT = "SELECT COUNT(*) FROM ads";

    /**
     * String Value used to count ads by categorie
     */
    public static final String SELECT_COUNT_BYCATEGORIE = "SELECT COUNT(*) FROM ads WHERE idcategories = ?";

    /**
     * String Value used to count ads by Search
     */
    public static final String SELECT_COUNT_BYSEARCH = "SELECT COUNT(*) FROM ads WHERE body LIKE ?";


    /**
     * data access param init
     */
    @Resource(name = "jdbc/petitesannonces")
    private DataSource datasource;

    /**
     * @param datasource
     * Method used to initialize the data base access
     */
    public AdsDAO(DataSource datasource) {
        this.datasource = datasource;
    }

    /**
     *
     * @param results, the dataBase request's result
     * @return ads, java object representation of the request
     * @see imageStore.findAllbyIdAds()
     * @see userDao.findById()
     * @see categorierDao.findById()
     * @throws SQLException
     */
    protected Ads resultSetToAds (ResultSet results)
    throws SQLException {
        Ads ads = new Ads();
        ImageStoreDAO imageStore = new ImageStoreDAO(datasource);
        UserDAO userDao  = new UserDAO(this.datasource);
        CategoriesDAO categorierDao  = new CategoriesDAO(this.datasource);

        ads.setId(results.getLong(1));
        // Find all ads's images on database and set the java object representation
        ads.setImages(imageStore.findAllbyIdAds(results.getLong(1)));
        // Find ads's user on database and set the java object representation
        ads.setUser(userDao.findById(results.getLong(2)));
        // Find ads's categorie on database and set the java object representation
        ads.setCategories(categorierDao.findById(results.getLong(3)));
        ads.setTitle(results.getString(4));
        ads.setBody(results.getString(5));
        ads.setCreationDate(results.getTimestamp(6).toLocalDateTime());

        return ads;
    }

    /**
     *
     * @param id, Ads's id to be found on data Base
     * @return ads, value of Ads found by request
     * @throws SQLException
     */
    public Ads findById(long id) throws SQLException {
        Ads ads = null;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_BY_ID);
        ) {
            statement.setLong(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    ads = this.resultSetToAds(results);
                }
            }
        }

        return ads;
    }

    public void deleteById(long id) throws SQLException {

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(DELETE_BY_ID);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }

    /**
     *
     * @param page, page selected on _pagination.jsp
     * @param pageSize, count of ads to be displayed
     * @return adsList, List of Ads found
     * @throws SQLException
     */
    public List<Ads> findAll(int page, int pageSize) throws SQLException {
        List<Ads> adsList = new ArrayList<>();
        int first = (page - 1) * pageSize + 1;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_ALL);
        ) {
            statement.setInt(1, first-1);
            statement.setInt(2, pageSize);

            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    adsList.add(resultSetToAds(results));
                }
            }
        }

        return adsList;
    }

    /**
     *
     * @param page, page selected on _pagination.jsp
     * @param pageSize, count of ads to be displayed
     * @return adsList, List of Ads found
     * @throws SQLException
     */
    public List<Ads> findAllBySearch (int page, int pageSize, String search) throws SQLException {
        List<Ads> adsList = new ArrayList<>();
        int first = (page - 1) * pageSize + 1;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_ALL_BY_SEARCH);
        ) {
            statement.setString(1, "%" + search + "%");
            statement.setInt(2, first-1);
            statement.setInt(3, pageSize);

            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    adsList.add(resultSetToAds(results));
                }
            }
        }

        return adsList;
    }

    /**
     *
     * @param page, page selected on _pagination.jsp
     * @param pageSize, count of ads to be displayed
     * @param idCategorie, used to found all ads selected by categorie
     * @return adsList, List of Ads found
     * @throws SQLException
     */
    public List<Ads> findAllByCategorie(int page, int pageSize, long idCategorie) throws SQLException {
        List<Ads> adsList = new ArrayList<>();
        int first = (page - 1) * pageSize + 1;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_BY_IDCATEGORIE);
        ) {
            statement.setLong(1, idCategorie);
            statement.setInt(2, first-1);
            statement.setInt(3, pageSize);

            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    adsList.add(resultSetToAds(results));
                }
            }
        }

        return adsList;
    }


    /**
     *
     * @param idUser, id of user who creates ads
     * @param idCategories, categorie of Ads created
     * @param title, title of the Ads
     * @param body, Body of the Ads
     * @param date, Date of ads creation
     * @return, Ads id, used to update other table
     * @throws SQLException
     */
    public long saveAds(long idUser, long idCategories, String title, String body, String date) throws SQLException {
        Ads ads = new Ads();
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                INSERT,
                Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setLong(1, idUser);
            statement.setLong(2, idCategories);
            statement.setString(3, title);
            statement.setString(4, body);
            statement.setString(5, date);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Article creation failed, no rows affected");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    ads.setId(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException(
                        "Article creation failed, no id obtained");
                }
            }
        }
        return ads.getId();
    }

    /**
     *
     * @return the count of Ads on Data Base
     * @throws SQLException
     */
    public long getCount() throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_COUNT);
            ResultSet results = statement.executeQuery()
        ) {
            results.next();
            return results.getLong(1);
        }
    }

    /**
     *
     * @return the count of Ads on Data Base by selected categorie
     * @throws SQLException
     */
    public long getCountByCategorie(long id) throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_COUNT_BYCATEGORIE);
                )
        {
            statement.setLong(1, id);
            ResultSet results = statement.executeQuery();
            results.next();
            return results.getLong(1);
        }
    }

    /**
     *
     * @return the count of Ads on Data Base by selected categorie
     * @throws SQLException
     */
    public long getCountBySearch(String search) throws SQLException {

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
            connection.prepareStatement(SELECT_COUNT_BYSEARCH);
            )
            {
                statement.setString(1, "%" + search +"%");
                ResultSet results = statement.executeQuery();
                results.next();
            return results.getLong(1);
        }
    }
}
