package com.tovo.applications.annonces.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import com.tovo.applications.annonces.models.UserRole;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserRoleDAO {

    /**
     * String Value used to find all fields of role Table
     */
    private static final String FIELD_LIST =
        "idrole,"
        + "roletitle ";

    /**
     * String Value used to find role with a given id
     */
    private static final String SELECT_BY_ID =
        "SELECT "
            + FIELD_LIST
            + " FROM role WHERE idrole = ?";

    /**
     * String Value used to find role with a given roletitle
     */
    private static final String FIND_BY_ROLETITLE =
    "SELECT "
        + FIELD_LIST
        + " FROM role WHERE roleTitle like ?";

    /**
     * data access param init
     */
    @Resource(name = "jdbc/petitesannonces")
    private DataSource datasource;

    /**
     * @param datasource
     * Method used to initialize the data base access
     */
    public UserRoleDAO (DataSource datasource) {
        this.datasource = datasource;
    }

    /**
     *
     * @param results, the dataBase request's result
     * @return userRole
     * @throws SQLException
     */
    protected UserRole resultSetToUserRole(ResultSet results)
        throws SQLException {

        UserRole userRole = new UserRole();

        userRole.setId(results.getLong(1));
        userRole.setRoleTitle(results.getString(2));

        return userRole;
    }

    /**
     *
     * @param roleTitle, to be found on request
     * @return role
     * @throws SQLException
     */
    public long findByRoletitle(String roleTitle) throws SQLException {
        UserRole userRole = null;
        long id = 0;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(FIND_BY_ROLETITLE);
        ) {
            statement.setString(1, roleTitle);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    userRole = this.resultSetToUserRole(results);
                    id = userRole.getId();
                }
            }
        }

        return id;
    }

    /**
     *
     * @param id, role's id to find
     * @return role found
     * @throws SQLException
     */
    public UserRole findById(long id) throws SQLException {
        UserRole userRole = null;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_BY_ID);
        ) {
            statement.setLong(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    userRole = this.resultSetToUserRole(results);
                }
            }
        }

        return userRole;
    }
}
