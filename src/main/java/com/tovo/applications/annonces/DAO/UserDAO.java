package com.tovo.applications.annonces.DAO;

import com.tovo.applications.annonces.models.User;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2Factory.Argon2Types;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class UserDAO {

    /**
     * PEPPER used with Argon2
     */
    // private static final String PEPPER = System.getenv("PETITES_ANNONCES_APP_SECRET");
    private static final String PEPPER = "J07PPWPMUHtB62HoeJJJqwHobMUMihO2IZ895onxDxDM4qzi9hOTBU6cDlRzcYtuQzdmDjc1SYbgpcdgvNU6flDYnPtiHjaPNP7L";

    /**
     * String Value used to find all user of user Table
     */
    private static final String FIELD_LIST =
        "iduser,"
        + "idrole,"
        + "username,"
        + "password,"
        + "cookieToken,"
        + "cookieTokenExpiration ";

    /**
     * String Value used to find user with a given id
     */
    public static final String  SELECT_BY_ID =
    "SELECT "
    + FIELD_LIST
    + "FROM user WHERE iduser = ? AND isactive =1";

    /**
     * String Value used to delete (isactive = 0) user
     */
    public static String DELETE_BY_ID =
    "UPDATE user SET isactive = '0' WHERE IDUSER = ?;";

    /**
     * String Value used to find user with a given username
     */
    public static final String SELECT_USER_BY_USERNAME =
    "SELECT "
        + FIELD_LIST
        + "FROM user WHERE username = ? AND isactive ='1'";

    /**
     * String Value used to find user with a given cookie_token
     */
    public static final String SELECT_USER_BY_COOKIE_TOKEN =
        "SELECT "
            + FIELD_LIST
            + "FROM user WHERE cookieToken = ?";

    /**
     * String value used to update all user's fields
     */
    public static final String UPDATE_USER =
        "UPDATE user SET "
            + "idrole = ?,"
            + "username = ?,"
            + "password = ?,"
            + "cookietoken = ?,"
            + "cookietokenexpiration = ? "
            + "WHERE iduser = ?";

    /**
     * String value used to update user's role an username
     */
    public static final String UPDATE_USER_USERNAME =
    "UPDATE user SET "
        + "idrole = ?,"
        + "username = ?"
        + "WHERE iduser = ?";

    /**
     * String Value used to insert new user on Data base
     */
    public static final String INSERT =
        "INSERT INTO user (username, password) VALUES (?, ?);";

    /**
     * String Value used to insert new user on Data base
     */
    public static final String INSERT_WITH_ROLE =
        "INSERT INTO user (username, password, idrole) VALUES (?, ?, ?);";

    /**
     * String Value used to find all users
     */
    public static final String SELECT_ALL =
        "SELECT "
            + FIELD_LIST
            + "FROM user WHERE isactive = 1";

    /**
     * String Value used to count ads
     */
    public static final String SELECT_COUNT = "SELECT COUNT(*) FROM user";

    /**
     * data access param init
     */
    @Resource(name = "jdbc/petitesannonces")
    private DataSource datasource;

    /**
     * @param datasource
     * Method used to initialize the data base access
     */
    public UserDAO (DataSource datasource) {
        this.datasource = datasource;
    }

    /**
     *
     * @param results, the dataBase request's result
     * @return user, java object representation of the request
     * @see userRoleDao.findById()
     * @throws SQLException
     */
    private User resultSetToUser(ResultSet results) throws SQLException {
        User user = new User();

        user.setId(results.getLong(1));
        user.setUsername(results.getString(3));
        user.setPassword(results.getString(4));
        user.setCookieToken(results.getString(5));

        if (results.getTimestamp(6) != null) {
            user.setCookieTokenExpiration(
                results.getTimestamp(6).toLocalDateTime()
            );
        }

        UserRoleDAO userRoleDao  = new UserRoleDAO(this.datasource);
        user.setUserRole(userRoleDao.findById(results.getLong(2)));

        return user;
    }

    /**
     *
     * @return userList, List of users found bu request
     * @throws SQLException
     */
    public List<User> findAll() throws SQLException {
        List<User> userList = new ArrayList<>();

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_ALL);
        ) {
            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    userList.add(resultSetToUser(results));
                }
            }
        }
        return userList;
    }

    /**
     * @param username to be saved on database
     * @param password, encrypted password to be saved on database
     * @see argon2.hash()
     * @deprecated argon2.hash()
     * @throws SQLException
     */
    public void saveUser(String username, String password) throws SQLException {

        Argon2 argon2 = Argon2Factory.create(Argon2Types.ARGON2id);
        String encryptedPassword = argon2.hash(4,1024*1024,8, password + PEPPER);

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                INSERT,
                Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, username);
            statement.setString(2, encryptedPassword);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Article creation failed, no rows affected");
            }
        }
    }

    /**
     *
     * @param username
     * @param password
     * @param idRole
     * @deprecated argon2.hash()
     * @throws SQLException
     */
    public void saveUserWithRole(String username, String password, long idRole) throws SQLException {

        Argon2 argon2 = Argon2Factory.create(Argon2Types.ARGON2id);
        String encryptedPassword = argon2.hash(4,1024*1024,8, password + PEPPER);
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                INSERT_WITH_ROLE);
        ) {
            statement.setString(1, username);
            statement.setString(2, encryptedPassword);
            statement.setLong(3, idRole);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException(
                    "Article creation failed, no rows affected");
            }
        }
    }

    /**
     * Method used to delete user by id
     * @param id
     * @throws SQLException
     */
    public void deleteUser(long id) throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement
                = connection.prepareStatement(DELETE_BY_ID)
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }

    /**
     *
     * @param id, user's id to find
     * @return found user
     * @throws SQLException
     */
    public User findById(long id) throws SQLException {
        User user = null;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_BY_ID);
        ) {
            statement.setLong(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    user = this.resultSetToUser(results);
                }
            }
        }
        return user;
    }

    /**
     *
     * @param username, user's username to be found
     * @return foundUser
     * @throws SQLException
     */
    public User findUserByUsername (String username) throws SQLException {
        User foundUser;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                SELECT_USER_BY_USERNAME)
        ) {
            statement.setString(1, username);

            try ( ResultSet results = statement.executeQuery() ) {
                if (results.next()) {
                    foundUser = this.resultSetToUser(results);
                }
                else {
                    throw new SQLException(
                        String.format(
                            "User not found: %s",
                            username));
                }
            }
        }

        return foundUser;
    }

    /**
     * Method used to compare password
     * @param submittedPassword, encrypted password given by user
     * @param encryptedPassword, encrypted password given by database
     * @see argon2.verify()
     * @deprecated argon2.verify()
     * @return passwordMatches, boolean value
     */
    public static Boolean testPassword (String submittedPassword, String encryptedPassword){

        Argon2 argon2 = Argon2Factory.create(Argon2Types.ARGON2id);

        boolean passwordMatches = argon2.verify(encryptedPassword, submittedPassword + PEPPER);
        return passwordMatches;

    }

    /**
     * Methdo used to compare cookie given by user's browser and cookie on database
     * @param cookieAuthenticationToken, value of cookie given by user's browser
     * @return foundUser
     * @throws SQLException
     */
    public User findUserByCookieToken(
        String cookieAuthenticationToken
    ) throws SQLException {

        User foundUser = null;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                SELECT_USER_BY_COOKIE_TOKEN)
        ) {
            statement.setString(1, cookieAuthenticationToken);

            try ( ResultSet results = statement.executeQuery() ) {
                if (results.next()) {
                    foundUser = this.resultSetToUser(results);
                }
            }
        }

        return foundUser;
    }

    /**
     * Method used to update user on login or logout
     * @param user, to be updated on database
     * @throws SQLException
     */
    public void update(User user) throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement
                = connection.prepareStatement(UPDATE_USER)
        ) {
            statement.setLong(1, user.getUserRole().getId());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getCookieToken());
            if (user.getCookieTokenExpiration() != null) {
                Timestamp timestamp = Timestamp.valueOf(
                    user.getCookieTokenExpiration());
                    statement.setTimestamp(5, timestamp);
                }
                else {
                statement.setTimestamp(5, null);
            }
            statement.setLong(6, user.getId());
            statement.executeUpdate();
        }
    }

    /**
     * Method used to update user by adminPage
     * @param idRole, update role when user's role changed
     * @param username, update username when username changed
     * @param id, user's id to be updated
     * @throws SQLException
     */
    public void updateUsername (Long idRole, String username, long id) throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement
                = connection.prepareStatement(UPDATE_USER_USERNAME)
        ) {

            statement.setLong(1, idRole);
            statement.setString(2, username);
            statement.setLong(3, id);

            statement.executeUpdate();
        }
    }

    public long getCount() throws SQLException {
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_COUNT);
            ResultSet results = statement.executeQuery()
        ) {
            results.next();
            return results.getLong(1);
        }
    }
}
