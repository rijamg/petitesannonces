package com.tovo.applications.annonces.DAO;

import com.tovo.applications.annonces.models.ImageStore;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.sql.DataSource;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Slf4j
public class ImageStoreDAO {

    /**
     * String Value used to find all fields of image Table
     */
    private static final String FIELD_LIST =
        "idimage,"
        + "idads,"
        + "imagedescription ";

    /**
     * String Value used to find image with a given id
     */
    public static final String SELECT_BY_ID =
        "SELECT "
            + FIELD_LIST
            + "FROM image WHERE idimage = ?";

    public static final String DELETE_BY_ID =
        "DELETE FROM image WHERE idads = ?;";

    /**
     * String Value used to find images included on ads
     */
    public static final String SELECT_ALL_BY_IDADS =
        "SELECT "
        + FIELD_LIST
        + "FROM image WHERE idads = ?";

    /**
     * String Value used to insert new images to Data base
     */
    public static final String INSERT =
        "INSERT INTO image (idads, imagedescription) VALUES (?, ?)";

    /**
     * data access param init
     */
    @Resource(name = "jdbc/petitesannonces")
    private DataSource datasource;

    /**
     * @param datasource
     * Method used to initialize the data base access
     */
    public ImageStoreDAO (DataSource datasource) {
        this.datasource = datasource;
    }

    /**
     *
     * @param results, the dataBase request result
     * @return imageStore, java object representation of the request
     * @throws SQLException
     */
    protected ImageStore resultSetToImageStore(ResultSet results)
        throws SQLException {

        ImageStore imageStore = new ImageStore();

        imageStore.setId(results.getLong(1));
        imageStore.setImageDescription(results.getString(3));
        String path = imageStore.getImageDescription().replace("\\","\\");

        try {
            BufferedImage buffImage = ImageIO.read(new File(path));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( buffImage, "jpg", baos );
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            String encoded = Base64.getEncoder().encodeToString(imageInByte);
            imageStore.setPhoto(encoded);
            baos.close();
        } catch (IOException e) {
                e.printStackTrace();
            }

        return imageStore;
    }

    public void deleteById(long id) throws SQLException {

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(DELETE_BY_ID);
        ) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }

    /**
     *
     * @param idAds, id used to find all included images
     * @return List of image
     * @throws SQLException
     */
    public List<ImageStore> findAllbyIdAds(long idAds) throws SQLException {
        List<ImageStore> imageStoresList = new ArrayList<>();

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_ALL_BY_IDADS);
        ) {
            statement.setLong(1, idAds);

            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    imageStoresList.add(resultSetToImageStore(results));
                }
            }
        }

        return imageStoresList;
    }

    /**
     *
     * @param idAds, Ads 's id to be saved on database
     * @param path, the path of image stored on hard disk
     * @throws SQLException
     */
    public void saveImageStore(long idAds , String path) throws SQLException {
        ImageStore imageStore = new ImageStore();
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                INSERT,
                Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setLong(1, idAds);
            statement.setString(2, path);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Article creation failed, no rows affected");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    imageStore.setId(generatedKeys.getInt(1));
                }
                else {
                    throw new SQLException(
                        "Article creation failed, no id obtained");
                }
            }
        }
    }
}
