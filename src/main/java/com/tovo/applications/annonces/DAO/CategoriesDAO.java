package com.tovo.applications.annonces.DAO;


import com.tovo.applications.annonces.models.Categories;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CategoriesDAO {

    /**
     * String Value used to find all fields of Categorie Table
     */
    private static final String FIELD_LIST =
        "idcategories,"
        + "categorietitle ";

    /**
     * String Value used to find Categorie with a given id
     */
    private static final String SELECT_BY_ID =
        "SELECT "
            + FIELD_LIST
            + "FROM categories WHERE idcategories = ?";

    /**
     * String Value used to find all Categories
     */
    public static final String SELECT_ALL =
        "SELECT "
            + FIELD_LIST
            + "FROM categories";

    /**
     * String Value used to insert new Categorie to Data base
     */
    public static final String INSERT =
        "INSERT INTO categories (categorietitle) VALUES (?);";

    /**
     * data access param init
     */
    @Resource(name = "jdbc/petitesannonces")
    private DataSource datasource;

    /**
     * @param datasource
     * Method used to initialize the data base access
     */
    public CategoriesDAO (DataSource datasource) {
        this.datasource = datasource;
    }

    /**
     *
     * @param results, the dataBase request result
     * @return categorie, java object representation of the request
     * @throws SQLException
     */
    protected Categories resultSetToCategorie(ResultSet results)
        throws SQLException {

        Categories categorie = new Categories();

        categorie.setId(results.getLong(1));
        categorie.setCategorieTitle(results.getString(2));

        return categorie;
    }

    /**
     *
     * @param id, Categorie's id to be found on data Base
     * @return categorie, value of categorie found by request
     * @throws SQLException
     */
    public Categories findById(long id) throws SQLException {
        Categories categorie = null;

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_BY_ID);
        ) {
            statement.setLong(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if (results.next()) {
                    categorie = this.resultSetToCategorie(results);
                }
            }
        }

        return categorie;
    }


    /**
     *
     * @return, List of all Categories
     * @throws SQLException
     */
    public List<Categories> findAll() throws SQLException {
        List<Categories> categorieList = new ArrayList<>();

        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement =
                connection.prepareStatement(SELECT_ALL);
        ) {
            try (ResultSet results = statement.executeQuery()) {
                while (results.next()) {
                    categorieList.add(resultSetToCategorie(results));
                }
            }
        }

        return categorieList;
    }


    /**
     *
     * @param categorieTitle, name of new categorie to be saved
     * @throws SQLException
     */
    public void saveCategorie (String categorieTitle) throws SQLException {
        Categories newCategorie = new Categories();
        try (
            Connection connection = this.datasource.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                INSERT,
                Statement.RETURN_GENERATED_KEYS);
        ) {
            statement.setString(1, categorieTitle);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException(
                    "Categorie creation failed, no rows affected");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    newCategorie.setId(generatedKeys.getInt(1));
                    newCategorie.setCategorieTitle(categorieTitle);
                }
                else {
                    throw new SQLException(
                        "Categorie creation failed, no id obtained");
                }
            }
        }
    }
}
