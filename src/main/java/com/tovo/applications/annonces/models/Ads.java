package com.tovo.applications.annonces.models;


import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Bean create with Lombok for ADs
 */
@Data
public class Ads {
    private long id;
    private String title;
    private String body;
    private LocalDateTime creationDate;
    private User user;
    private Categories categories;
    private List<ImageStore> images;
}
