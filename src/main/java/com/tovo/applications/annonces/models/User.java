package com.tovo.applications.annonces.models;

import java.time.LocalDateTime;

import lombok.Data;



/**
 * Bean create with Lombok for user
 */
@Data
public class User {
    private long id;
    private String username;
    private String password;
    private String cookieToken;
    private LocalDateTime cookieTokenExpiration;
    private UserRole userRole;
}
