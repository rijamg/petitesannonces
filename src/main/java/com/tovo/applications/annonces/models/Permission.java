package com.tovo.applications.annonces.models;

import lombok.Data;

/**
 * Bean create with Lombok for permission
 */
@Data
public class Permission {
    private long id;
    private String permissionKey;
}
