package com.tovo.applications.annonces.models;

import lombok.Data;


/**
 * Bean create with Lombok for role
 */
@Data
public class UserRole {
    private long id;
    private String roleTitle;
}
