package com.tovo.applications.annonces.models;


import lombok.Data;

/**
 * Bean create with Lombok for Categories
 */
@Data
public class Categories {
    private long id;
    private String categorieTitle;
}
