package com.tovo.applications.annonces.models;


import lombok.Data;

/**
 * Bean create with Lombok for image
 */
@Data
public class ImageStore {
    private long id;
    private String imageDescription;
    private String photo;
}
