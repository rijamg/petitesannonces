package com.tovo.applications.annonces.models;

import lombok.Data;

/**
 * Bean create with Lombok for ACL
 */
@Data
public class Acl {
    UserRole role;
    Permission permission;
}
