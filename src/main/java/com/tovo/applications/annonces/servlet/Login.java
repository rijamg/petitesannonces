package com.tovo.applications.annonces.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tovo.applications.annonces.DAO.UserDAO;
import com.tovo.applications.annonces.models.User;
import com.tovo.applications.annonces.util.CookieHelper;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet("/login")
public class Login extends HttpServlet{


    private static final long serialVersionUID = 2074398220377111808L;

    /**
     * Database access init
     */
    private UserDAO userDAO;

    /**
     * Database access init
     */
    @Resource(name = "jdbc/petitesannonces")
    javax.sql.DataSource datasource;

    /**
     * Database access init with DAO
     */
    @Override
    public void init() throws ServletException {
        this.userDAO = new UserDAO(this.datasource);
    }

    /**
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     * page to display login.jsp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/pages/login.jsp").forward(req, resp);
    }

    /**
     * find username and password
     * find user by username
     * @see this.userDAO.findUserByUsername()
     * test password
     * @see UserDAO.testPassword()
     * give cookie and cookie expiration date if stay-logged-on
     * update user with cookie and cookie expiration date
     * when user have made a destination choice before login, resend user to destination
     * @param req
     * @param resp
     * @deprecated argon2
     * @throws ServletException
     * @throws IOException
     * page to display login.jsp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = null;
        Boolean testPassWord = false;

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        try {
            user = this.userDAO.findUserByUsername(username);
            testPassWord = UserDAO.testPassword(password , user.getPassword());
        }
        catch (SQLException sqle) {

            req.setAttribute("message", sqle);
            req.getRequestDispatcher("WEB-INF/jsp/pages/notPermitted.jsp")
                    .forward(req, resp);
        }

        if (testPassWord){
            HttpSession session = req.getSession();
            session.setAttribute("user", user);

            if (req.getParameter("stay-logged-in") != null
                && req.getParameter("stay-logged-in").equals("true")) {

                String token = CookieHelper.generateToken();
                resp.addCookie(CookieHelper.createAuthTokenCookie(token));

                user.setCookieToken(token);
                user.setCookieTokenExpiration(
                    LocalDateTime.now().plusSeconds(CookieHelper.AUTH_TOKEN_LIFESPAN)
                );

                System.out.println();


                try {
                    userDAO.update(user);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (req.getParameter("dest") != null) {
                resp.sendRedirect(req.getContextPath() + req.getParameter("dest"));
            }
            else {
                req.getRequestDispatcher("WEB-INF/jsp/pages/login.jsp")
                    .forward(req, resp);
            }
        }else{

            String message = "password not correct";
                req.setAttribute("message", message);
                req.getRequestDispatcher("/WEB-INF/jsp/pages/notPermitted.jsp").forward(req, resp);
        }
    }

}
