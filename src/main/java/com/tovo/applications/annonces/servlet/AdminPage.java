package com.tovo.applications.annonces.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.tovo.applications.annonces.DAO.CategoriesDAO;
import com.tovo.applications.annonces.DAO.UserDAO;

import lombok.extern.slf4j.Slf4j;


@Slf4j @WebServlet("/adminPage")
public class AdminPage extends HttpServlet{

    /**
     * init database
     */
    private CategoriesDAO categorieDAO;
    /**
     * init database DAO
     */
    private UserDAO userDAO;

    /**
     * init database DAO
     */
    @Resource(name =  "jdbc/petitesannonces")
    DataSource datasource;

    /**
     * init database DAO
     */
    @Override
    public void init() throws ServletException {
        this.categorieDAO = new CategoriesDAO(datasource);
        this.userDAO = new UserDAO(datasource);
    }


    private static final long serialVersionUID = 1L;
    /**
     * Get userList to display on page adminPage.jsp
     * @see this.userDAO.findAll()
     * delete user if button is selected
     * @see userDAO.deleteUser()
     *@param req
     *@param resp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            req.setAttribute("userList", this.userDAO.findAll());
        } catch (SQLException sqle) {
            log.debug("try to req All User failed : "  + sqle );
        }

        if (req.getParameter("id") != null){
            long idUser = Long.parseLong(req.getParameter("id"));
            try {
                userDAO.deleteUser(idUser);
            } catch (SQLException sqle) {
                log.debug("trying statement" + sqle.getMessage());
            }
        }

        req.getRequestDispatcher("/WEB-INF/jsp/pages/adminPage.jsp").forward(req, resp);
    }

    /**
     * add new categorie
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String newCategorie = (String) req.getParameter("categorie");

        if (newCategorie != null){
            try {
                categorieDAO.saveCategorie(newCategorie);
            } catch (SQLException sqle) {
                log.debug("trying statement" + sqle.getMessage());
            }

            req.getRequestDispatcher("/WEB-INF/jsp/pages/adminPage.jsp").forward(req, resp);
        }
    }

}
