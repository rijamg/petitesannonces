package com.tovo.applications.annonces.servlet;

import com.tovo.applications.annonces.DAO.AdsDAO;
import com.tovo.applications.annonces.DAO.CategoriesDAO;
import com.tovo.applications.annonces.DAO.UserDAO;
import com.tovo.applications.annonces.models.Ads;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("")
@Slf4j
public class HomePage extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Java object value for ads on database
     */
    public static List<Ads> adsList = new ArrayList<>();
    /**
     * number of ads to display on pages
     */
    private static final int ITEMS_PER_PAGE = 6;

    /**
     * init DAO
     */
    private CategoriesDAO categoriesDAO;
    private AdsDAO adsDAO;
    private UserDAO userDAO;

    /**
     * Database access init
     */
    @Resource(name = "jdbc/petitesannonces")
    DataSource dataSource;

    /**
     * Database access init with DAO
     * @deprecated
     */
    @Override
    public void init() throws ServletException {
        this.categoriesDAO = new CategoriesDAO(this.dataSource);
        this.adsDAO = new AdsDAO(this.dataSource);
        this.userDAO = new UserDAO(this.dataSource);

        try {
            if (this.userDAO.getCount() == 0L) {
                userDAO.saveUserWithRole("admin", "secret", 2L);
            }
        } catch (SQLException e) {
            log.debug("SQLException {}", e);
        }
    }

    /**
     * Find page selected
     * if categorie is choose then display only ads with categorie
     * else display all ads
     * page to diplay : index.jsp
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     *
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int page = pageSearch(req, resp);

        req.setAttribute("itemsPerPage", ITEMS_PER_PAGE);
        req.setAttribute("currentPage", page);

        // if already set a category
        if (req.getParameter("idCategorie") != null && !req.getParameter("idCategorie").isEmpty())  {
            long idCategorie = Long.parseLong(req.getParameter("idCategorie"));
            try {
                req.setAttribute("articleCount", this.adsDAO.getCountByCategorie(idCategorie));
                req.setAttribute("adsList", this.adsDAO.findAllByCategorie(page, ITEMS_PER_PAGE, idCategorie));
                req.setAttribute("idCategorie", req.getParameter("idCategorie"));
            } catch (SQLException sqle) {
                throw new ServletException(sqle);
            }

        } else {
            try {
                req.setAttribute("articleCount", this.adsDAO.getCount());
                req.setAttribute("adsList", this.adsDAO.findAll(page, ITEMS_PER_PAGE));
            } catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
        }
        HttpSession session = req.getSession();
        try {
            session.setAttribute("categories", this.categoriesDAO.findAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        searchAds(req, resp, page);

        req.getRequestDispatcher("/WEB-INF/jsp/pages/index.jsp").forward(req, resp);
    }

    /**
     * Search all ads with pages by search
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int page = 1;

        searchAds(req, resp, page);

        req.getRequestDispatcher("/WEB-INF/jsp/pages/index.jsp").forward(req, resp);
    }

    /**
     * Search ads, if first search, take searchPost given by post Method on _header.jsp
     * else take search given by _pagination.jsp
     * @param req
     * @param resp
     * @param page
     * @throws ServletException
     * @throws IOException
     */
    protected void searchAds(HttpServletRequest req, HttpServletResponse resp, int page) throws ServletException, IOException {

        req.setAttribute("itemsPerPage", ITEMS_PER_PAGE);
        req.setAttribute("currentPage", page);
        if (req.getParameter("searchPost") != null && !req.getParameter("searchPost").isEmpty()){
            try {
                req.setAttribute("articleCount", this.adsDAO.getCountBySearch(req.getParameter("searchPost")));
                req.setAttribute("adsList", this.adsDAO.findAllBySearch(page, ITEMS_PER_PAGE, req.getParameter("searchPost")));
                req.setAttribute("search", req.getParameter("searchPost"));
            } catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
        } else if (req.getParameter("search") != null && !req.getParameter("search").isEmpty()){
            try {
                req.setAttribute("articleCount", this.adsDAO.getCountBySearch(req.getParameter("search")));
                req.setAttribute("adsList", this.adsDAO.findAllBySearch(page, ITEMS_PER_PAGE, req.getParameter("search")));
                req.setAttribute("search", req.getParameter("search"));
            } catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
        }
    }

    /**
     * Give the page sent by _pagination.jsp
     * @param req
     * @param resp
     * @return
     */
    protected int pageSearch (HttpServletRequest req, HttpServletResponse resp) {
        int page;

        try {
            page = req.getParameter("page") != null ? Integer.parseInt(req.getParameter("page")) : 1;
            if (page < 0) {
                page = 1;
            }
        } catch (NumberFormatException nfe) {
            page = 1;
        }

        return page;
    }


}
