package com.tovo.applications.annonces.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.tovo.applications.annonces.DAO.UserDAO;
import com.tovo.applications.annonces.models.User;
import com.tovo.applications.annonces.util.CookieHelper;




@WebServlet("/logout")
public class Logout extends HttpServlet{

    private static final long serialVersionUID = 1L;
    /**
     * data access param init
     */
    private UserDAO userDAO;

    /**
     * data access param init
     */
    @Resource(name="jdbc/petitesannonces")
    DataSource datasource;

    /**
     * data access param init
     */
    @Override
    public void init() throws ServletException {
        this.userDAO = new UserDAO(this.datasource);
    }

    /**
     * get user by session value
     * remove session attribute user
     * remove cookie and cookie expiration date
     * @see CookieHelper.createAuthTokenCookieDeletion()
     * save user
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") != null) {
            User user = (User) req.getSession().getAttribute("user");
            req.getSession().removeAttribute("user");
            resp.addCookie(CookieHelper.createAuthTokenCookieDeletion());
            user.setCookieToken(null);
            user.setCookieTokenExpiration(null);

            try {
                this.userDAO.update(user);
            }
            catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
        }
        resp.sendRedirect(req.getContextPath() + "/login");
    }
}
