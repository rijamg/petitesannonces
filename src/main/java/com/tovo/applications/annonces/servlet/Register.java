package com.tovo.applications.annonces.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.tovo.applications.annonces.DAO.UserDAO;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet("/register")
public class Register extends HttpServlet {


    private static final long serialVersionUID = 1733790421713943037L;
    /**
     * init database
     */
    private UserDAO userDAO;

    /**
     * init database
     */
    @Resource(name = "jdbc/petitesannonces")
    DataSource datasource;

    /**
     * init database with DAO
     */
    @Override
    public void init() throws ServletException {
        this.userDAO = new UserDAO(this.datasource);
    }

    /**
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     * page to display register.jsp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/pages/register.jsp").forward(req, resp);
    }

    /**
     * fing username and password send by user
     * saveUser
     * @see userDAO.saveUser()
     * @param req
     * @param resp
     * @deprecated argon2
     * @throws ServletException
     * @throws IOException
     * page to display index.jsp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        try {
            userDAO.saveUser(username, password);
        }catch (SQLException sqle){

            log.debug("trying statement" + sqle.getMessage());
        }

        resp.sendRedirect(req.getContextPath() + "/");
    }

}
