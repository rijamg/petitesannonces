package com.tovo.applications.annonces.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tovo.applications.annonces.DAO.AdsDAO;
import com.tovo.applications.annonces.DAO.ImageStoreDAO;

import lombok.extern.slf4j.Slf4j;


@WebServlet("/vuead") @Slf4j
public class VueAds extends HttpServlet{

    private static final long serialVersionUID = 1L;
    /**
     * init Database
     */
    private AdsDAO adsDao;
    private ImageStoreDAO imageStoreDao;
    /**
     * init Database
     */
    @Resource(name="jdbc/petitesannonces")
    DataSource datasource;
    /**
     * init Database with DAO
     */
    @Override
    public void init() throws ServletException {
        this.adsDao = new AdsDAO(this.datasource);
        this.imageStoreDao = new ImageStoreDAO(this.datasource);
    }

    /**
     * find ADs by id selected
     * display ads
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     * page to display vueAds.jsp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("id") != null && !req.getParameter("id").isEmpty()) {
            long id = Long.parseLong(req.getParameter("id"));

            try {
                req.setAttribute("ads", this.adsDao.findById(id));
            }catch (SQLException sqle) {
                throw new ServletException(sqle);
            }
            req.getRequestDispatcher("/WEB-INF/jsp/pages/vueAd.jsp").forward(req, resp);
        }

        if (req.getParameter("idtodelete") != null && !req.getParameter("idtodelete").isEmpty()) {
            long idToDelete = Long.parseLong(req.getParameter("idtodelete"));
            try {
                this.imageStoreDao.deleteById(idToDelete);
                this.adsDao.deleteById(idToDelete);
            } catch (SQLException sqle) {
                log.debug("Try to delete {} id: {}", sqle, idToDelete);
            }
            resp.sendRedirect(req.getContextPath() + "/");
        }
    }

}
