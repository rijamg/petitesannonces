package com.tovo.applications.annonces.servlet;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;

import com.tovo.applications.annonces.DAO.AdsDAO;
import com.tovo.applications.annonces.DAO.ImageStoreDAO;
import com.tovo.applications.annonces.models.User;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@MultipartConfig
@WebServlet(name="createAds", urlPatterns = "/createAds")
public class CreateAds extends HttpServlet {


    private static final long serialVersionUID = -5218518153142232811L;

    /**
     * init Database
     */
    @Resource(name = "jdbc/petitesannonces")
    DataSource datasource;
    /**
     * init Database
     */
    AdsDAO adsDAO;
    /**
     * init Database
     */
    ImageStoreDAO imageStoreDAO;

    /**
     * Format date to send on database
     */
    private static final DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     * init Database with DAO
     */
    @Override
    public void init() throws ServletException {
        this.adsDAO = new AdsDAO(datasource);
        this.imageStoreDAO = new ImageStoreDAO(datasource);
    }

    /**
     * Display page createAds.jsp
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/pages/createAds.jsp").forward(req, resp);
    }


    /**
     * find user on session
     * save ads, image, categorie, user
     * page to send : Homepage.java
     * @param req
     * @param resp
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // get user on session
        User userSession = (User) req.getSession().getAttribute("user");
        // get user id
        long iduser = userSession.getId();
        // get ads categorie
        long idCategorie = Long.parseLong(req.getParameter("categories"));
        //get ads title
        String title = req.getParameter("adsTitle");
        //get ads body
        String body = req.getParameter("adsBody");
        // get date
        LocalDateTime date = LocalDateTime.now();

        long key = 0;

        // save Ads and return idAds used to save image
        try {
            key = this.adsDAO.saveAds(iduser, idCategorie, title, body, formatter.format(date));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // retrieve all images send by user
        List<Part> fileParts = req.getParts().stream().filter(part -> "photos".equals(part.getName()) && part.getSize() > 0).collect(Collectors.toList()); // Retrieves <input type="file" name="file" multiple="true">
        // retrive image for images
        for (Part filePart : fileParts) {
            // use user.home to have a path set bu user configuration
            String homePath = System.getProperty("user.home");
            // Build time used on path to avoid same path
            String time = LocalDateTime.now().toString().replace(":","_");
            // build Path
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
            String newfilename = time + fileName;
            Path newPath = Paths.get(homePath, "Documents", "repo_git","ecf3-jee","petitesAnnonce","Conception","server", newfilename);

            // Save all images with given path
            OutputStream out = null;
            InputStream filecontent = filePart.getInputStream();

            try{
                out = new FileOutputStream(new File(newPath.toString()));

                int read = 0;
                final byte[] bytes = new byte[1024];
                while ((read = filecontent.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }

            }catch (FileNotFoundException fne) {
                log.error(fne.toString());
            }
            finally {
                if (out != null) {
                    out.close();
                }
                if (filecontent != null) {
                    filecontent.close();
                }
            }
            // Save images with ads id and the path on database
            try {
                String pathToSave = newPath.toString();
                this.imageStoreDAO.saveImageStore(key, pathToSave);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        resp.sendRedirect(req.getContextPath() + "/");

    }
}
