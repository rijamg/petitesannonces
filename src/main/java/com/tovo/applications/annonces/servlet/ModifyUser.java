package com.tovo.applications.annonces.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.tovo.applications.annonces.DAO.UserDAO;
import com.tovo.applications.annonces.DAO.UserRoleDAO;
import com.tovo.applications.annonces.models.User;

import lombok.extern.slf4j.Slf4j;



@Slf4j @WebServlet("/modifyUser")
public class ModifyUser extends HttpServlet{

    private static final long serialVersionUID = 1L;

    /**
     * init database
     */
    private UserDAO userDAO;
    /**
     * init database
     */
    private UserRoleDAO userRoleDAO;

    /**
     * init database
     */
    @Resource(name =  "jdbc/petitesannonces")
    DataSource datasource;

    /**
     * init database with DAO
     */
    @Override
    public void init() throws ServletException {
        this.userDAO = new UserDAO(datasource);
        this.userRoleDAO = new UserRoleDAO(datasource);
    }

    /**
     * find user by id
     * send user to page modifyUser.jsp
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));
        User user = new User();
        try {
            user = userDAO.findById(id);
        } catch (SQLException sqle) {
            log.debug("trying statement" + sqle.getMessage());
        }

        req.setAttribute("user", user);
        req.getRequestDispatcher("WEB-INF/jsp/pages/modifyUser.jsp").forward(req, resp);
    }

    /**
     * find user by id send by post method on modifyUser.jsp
     * Update user with parameter send by user
     * page to display /adminPage
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));
        String username = req.getParameter("username");
        String roleTitle = req.getParameter("roleTitle");
        long idRole = 0;

        try {
            idRole = userRoleDAO.findByRoletitle(roleTitle);
        } catch (SQLException sqle) {
            String message = "Role not found";
                req.setAttribute("message", message);
                req.getRequestDispatcher("/WEB-INF/jsp/pages/notPermitted.jsp").forward(req, resp);
        }

        try {
            userDAO.updateUsername(idRole, username, id);
        } catch (SQLException sqle) {
            String message = "Change not effective";
                req.setAttribute("message", message);
                req.getRequestDispatcher("/WEB-INF/jsp/pages/notPermitted.jsp").forward(req, resp);
        }

        resp.sendRedirect(req.getContextPath() + "/adminPage");
    }
}
