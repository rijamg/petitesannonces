# ECF3 - JEE

## MISE EN PLACE DE L'ENVIRONNEMENT DE TEST
1 - Créer la base de donnée (schema.sql)
2 - Créer les premières données (dataTest.sql)
3 - au lancement de l'application
> Création par le programme d'un utilisateur: "admin" mdp: "secret".

4 - S'enregistrer à l'application avec le bouton enregistrement
  - Création de l'utilisateur pour pouvoir ajouter des annonces
5 - Pour voir la page admin
> Se connecter en tant qu'admin
> Saisir : localhost:8080/petitesAnnonce/adminPage
6 - Bonne Continuation

## CE QUI ETAIT DEMANDE ET LES SOLUTIONS PROPOSEES

* Gestion des utilisateurs (CRUD Complet) Lister les utilisateurs, créer les utilisateurs, modifier, supprimer
    * Username
    * Password
    * Cookie

    MODELS : User.java UserRole.java (ACL pas traité)
    DAO : UserDAO.java UserRoleDAO.java
    FILTRE : AuthentificationFilter.java CookieFilter.java
    SERVLET : Register.java Login.java Logout.java AdminPage.java
    JSP : register.jsp login.jsp adminPage.jsp

    CREATE : Page Register colonne isactive=1, (Toutes les recherches se font en regardant isactive=1)
    READ : All pages puisque present dans la session
    UPDATE : modifyUser (Modifier username et role)
    DELETE : adminPage, mise en place d'une colonne isactive dans user (par défaut = 1) et qu'on met à 0 quand on delete

* Authentifier les utilisateurs (avec session) (Cryptage du mot de passe Argon2 - SHA256)
  Authentification : login et par cookie (UserDAO)

* Protection du mot de passe, sel poivre, hashage (sha1/sha256, bcrypt, argon2)
  Utilisation Argon2 (UserDAO)

* Protection CSRF
Application de JSTL avec Expressions Languages (c:out) partout ou on affiche les resultats venant de la BDD

* Protection XSS et autres injections (TokenCsrf)
Application de TokenCsrf dans tous les HTMLform

* wyswig
Mise en place de ckEditor

* Système de gestion de contenu
    * Aux moins 3 modèles (Hors User)
    * Sujet : Petites Annonces

# TO-DO
A Faire                             |Etat   |Date
|---------                          |-------|---
| Créer projet Jee                  |OK     |22/07/2020
| .gitignore                        |OK     |22/07/2020
| .editorconfig                     |OK     |22/07/2020
| Mettre à jour le .pom             |OK     |22/07/2020
| context.xml                       |OK     |22/07/2020
| web.xml                           |OK     |22/07/2020
| Depot Git                         |OK     |22/07/2020
| Branch Develop                    |OK     |22/07/2020
| Analyse des entités               |OK     |22/07/2020
| Bootstrap                         |OK     |22/07/2020
| Maquettage                        |OK     |22/07/2020
| Ajouter enregistrement cookie     |OK     |23/07/2020
| Créer BDD                         |OK     |23/07/2020
| Lombok et logback                 |OK     |23/07/2020
| Ajouter Date Création Ads         |OK     |23/07/2020
| Diagramme Pages/action            |AF     |23/07/2020
| Model                             |AF     |23/07/2020
| DAO                               |AF     |23/07/2020
| Footer                            |OK     |23/07/2020
| Header                            |OK     |23/07/2020
| Peupler BD                        |OK     |30/07/2020
| Homepage                          |OK     |01/08/2020
| createAds                         |OK     |01/08/2020
| Fonction modify/delete 2 page     |OK     |02/08/2020
| Javadoc                           |OK     |03/08/2020
| Commentaire                       |OK     |03/08/2020
| Problem after search              |OK     |04/08/2020
| Junit Test                        |       |
| QUILL                             |       |

## AMELIORATION
Stocker les ADS dans ads.java


## FONCTIONNALITES
### FAIT
* INSCRIPTION
* LOGIN - LOGOUT
* DEPOSER DES ANNONCES
* CONSULTER LES ANNONCES
* SEUL l'admin peut rajouter des categories
* Recherche par categories
* Recherche par mots clés
### A FAIRE
* MODIFIER DES ANNONCES QUAND ON EST CONNECTE ET SEULEMENT MES ANNONCES

## CONCEPTION

### DIAGRAMME UML

### DIAGRAMME DE CLASSES

## PAGES
* inscription
![alt](Conception/src/resources/inscription.PNG)
* Login (Logout - juste un lien)
![alt](Conception/src/resources/login.PNG)
* Consulter la liste des annonces (tout le monde)
![alt](Conception/src/resources/listeAds.PNG)
* Consulter une annonce particulière (tout le monde)
![alt](Conception/src/resources/oneAds.PNG)
* Depot d'annonces (Doit être connecté)
![alt](Conception/src/resources/createAds.PNG)
* Liste User
![alt](Conception/src/resources/listUser.PNG)


