drop schema if exists petitesannonces;
create schema petitesannonces;
use petitesannonces;

SET FOREIGN_KEY_CHECKS=0;
drop table if exists USER;
drop table if exists ROLE;
drop table if exists PERMISSION;
drop table if exists IMAGE;
drop table if exists CATEGORIES;
drop table if exists ACL;
drop table if exists ADS;
SET FOREIGN_KEY_CHECKS=1;

/*==============================================================*/
/* Table : ADS                                                  */
/*==============================================================*/
create table ADS
(
   IDADS                bigint not null AUTO_INCREMENT,
   IDUSER               bigint not null,
   IDCATEGORIES         bigint not null,
   TITLE                varchar(1024) not null,
   BODY                 text not null,
   CREATIONDATE         datetime not null,
   primary key (IDADS)
);

/*==============================================================*/
/* Table : ACL                                        */
/*==============================================================*/
create table ACL
(
   IDROLE               bigint not null AUTO_INCREMENT,
   IDPERMISSION         bigint not null,
   primary key (IDROLE, IDPERMISSION)
);

/*==============================================================*/
/* Table : CATEGORIES                                           */
/*==============================================================*/
create table CATEGORIES
(
   IDCATEGORIES         bigint not null AUTO_INCREMENT,
   CATEGORIETITLE       varchar(1024) not null,
   primary key (IDCATEGORIES)
);

/*==============================================================*/
/* Table : IMAGE                                                */
/*==============================================================*/
create table IMAGE
(
   IDIMAGE              bigint not null AUTO_INCREMENT,
   IDADS                bigint not null,
   IMAGEDESCRIPTION     varchar(1024) not null,
   primary key (IDIMAGE)
);



/*==============================================================*/
/* Table : PERMISSION                                           */
/*==============================================================*/
create table PERMISSION
(
   IDPERMISSION         bigint not null AUTO_INCREMENT,
   PERMISSIONKEY        varchar(1024),
   primary key (IDPERMISSION)
);

/*==============================================================*/
/* Table : ROLE                                                 */
/*==============================================================*/
create table ROLE
(
   IDROLE               bigint not null AUTO_INCREMENT,
   ROLETITLE            varchar(1024) not null,
   primary key (IDROLE)
);

/*==============================================================*/
/* Table : USER                                                 */
/*==============================================================*/
create table USER
(
   IDUSER               bigint not null AUTO_INCREMENT,
   IDROLE               bigint not null,
   USERNAME             varchar(50) not null UNIQUE,
   PASSWORD             varchar(100) not null,
   COOKIETOKEN          varchar(1024),
   COOKIETOKENEXPIRATION datetime,
   primary key (IDUSER)
);


alter table ADS add constraint FK_IS foreign key (IDCATEGORIES)
      references CATEGORIES (IDCATEGORIES) on delete restrict on update restrict;

alter table ADS add constraint FK_PLACE_AN_AD foreign key (IDUSER)
      references USER (IDUSER) on delete restrict on update restrict;

alter table ACL add constraint FK_ACL foreign key (IDROLE)
      references ROLE (IDROLE) on delete restrict on update restrict;

alter table ACL add constraint FK_ASSOCIATION_6 foreign key (IDPERMISSION)
      references PERMISSION (IDPERMISSION) on delete restrict on update restrict;

alter table IMAGE add constraint FK_ASSOCIATION_3 foreign key (IDADS)
      references ADS (IDADS) on delete cascade on update cascade;

alter table USER add constraint FK_ASSOCIATION_4 foreign key (IDROLE)
      references ROLE (IDROLE) on delete restrict on update restrict;

ALTER TABLE `petitesannonces`.`user`
DROP FOREIGN KEY `FK_ASSOCIATION_4`;
ALTER TABLE `petitesannonces`.`user`
CHANGE COLUMN `IDROLE` `IDROLE` BIGINT NOT NULL DEFAULT 3 ;
ALTER TABLE `petitesannonces`.`user`
ADD CONSTRAINT `FK_ASSOCIATION_4`
  FOREIGN KEY (`IDROLE`)
  REFERENCES `petitesannonces`.`role` (`IDROLE`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

ALTER TABLE `petitesannonces`.`user`
ADD COLUMN `ISACTIVE` TINYINT NOT NULL DEFAULT 1 AFTER `COOKIETOKENEXPIRATION`;
