INSERT INTO `petitesannonces`.`role` (`ROLETITLE`) VALUES ('superadmin');
INSERT INTO `petitesannonces`.`role` (`ROLETITLE`) VALUES ('admin');
INSERT INTO `petitesannonces`.`role` (`ROLETITLE`) VALUES ('user');



INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('create_user');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('read_user');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('update_user');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('delete_user');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('create_ads');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('read_ads');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('update_ads');
INSERT INTO `petitesannonces`.`permission` (`PERMISSIONKEY`) VALUES ('delete_ads');

INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '1');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '2');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '3');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '4');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '5');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '6');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '7');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('1', '8');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('2', '5');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('2', '6');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('2', '7');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('2', '8');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('3', '5');
INSERT INTO `petitesannonces`.`acl` (`IDROLE`, `IDPERMISSION`) VALUES ('3', '6');

INSERT INTO `petitesannonces`.`categories` (`CATEGORIETITLE`) VALUES ('Fruits');
INSERT INTO `petitesannonces`.`categories` (`CATEGORIETITLE`) VALUES ('Legumes');
INSERT INTO `petitesannonces`.`categories` (`CATEGORIETITLE`) VALUES ('Viandes');
INSERT INTO `petitesannonces`.`categories` (`CATEGORIETITLE`) VALUES ('Oeufs');


